package com.example.ivproject.repository;

import com.example.ivproject.entity.InvoiceHasItem;
import com.example.ivproject.entity.InvoiceHasItemId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceHasItemRepository extends JpaRepository<InvoiceHasItem, InvoiceHasItemId> {
    List<InvoiceHasItem> findByInvoiceId(long invoiceId);
}
