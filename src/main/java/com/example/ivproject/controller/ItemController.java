package com.example.ivproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/item")
public class ItemController {

    @GetMapping("/add")
    public Object addItem(ModelAndView modelAndView){

        modelAndView.setViewName("add_item");
        return modelAndView;
    }
}
