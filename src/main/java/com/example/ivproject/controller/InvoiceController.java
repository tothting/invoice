package com.example.ivproject.controller;

import com.example.ivproject.entity.Invoice;
import com.example.ivproject.entity.InvoiceHasItem;
import com.example.ivproject.entity.InvoiceSummary;
import com.example.ivproject.entity.Item;
import com.example.ivproject.repository.InvoiceHasItemRepository;
import com.example.ivproject.repository.InvoiceRepository;
import com.example.ivproject.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class InvoiceController {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    InvoiceHasItemRepository invoiceHasItemRepository;

    @GetMapping("/")
    public Object firstPage(ModelAndView modelAndView) {
        modelAndView.addObject("invoices", invoiceRepository.findAll());
        modelAndView.setViewName("index");
        return modelAndView;

    }

    @GetMapping("/all_item")
    public Object allItem(ModelAndView modelAndView) {
        modelAndView.addObject("items",itemRepository.findAll());
        modelAndView.setViewName("item");
        return modelAndView;
    }


    @GetMapping(value = "/invoice", params = "id")
    public Object getItemsByInvoiceId(@RequestParam long id, ModelAndView modelAndView) {

        InvoiceSummary invoiceSummary = new InvoiceSummary();


        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if(invoice == null){
            invoiceSummary.setInvoice(null);
        }
        else{
            invoiceSummary.setInvoice(invoice.get());
        }

        List<InvoiceHasItem> items = invoiceHasItemRepository.findByInvoiceId(id);
        invoiceSummary.setItemsList(items);
        List<Long> idList = new ArrayList<>();
        items.forEach(item -> {
            idList.add(item.getItemId());
        });
        invoiceSummary.setItems(itemRepository.findAllById(idList));


        modelAndView.addObject("invoiceSummary",invoiceSummary);
        modelAndView.setViewName("invoice");

        return modelAndView;
    }




}
