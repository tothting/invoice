package com.example.ivproject.entity;

import java.util.List;

public class InvoiceSummary {
    private Invoice invoice;
    private List<Item> items;
    private List<InvoiceHasItem> itemsList;

    public InvoiceSummary(){

    }

    public InvoiceSummary(Invoice invoice, List<Item> items, List<InvoiceHasItem> itemsList) {
        this.invoice = invoice;
        this.items = items;
        this.itemsList = itemsList;
    }

    public List<InvoiceHasItem> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<InvoiceHasItem> itemsList) {
        this.itemsList = itemsList;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
