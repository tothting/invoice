package com.example.ivproject.entity;

import java.io.Serializable;

public class InvoiceHasItemId implements Serializable {
    long invoiceId;
    long itemId;
}
