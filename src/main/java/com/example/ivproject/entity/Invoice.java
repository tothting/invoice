package com.example.ivproject.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Invoice {

  @Id
  private long id;
  private java.sql.Timestamp timestamp;
  private String customer;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public java.sql.Timestamp getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(java.sql.Timestamp timestamp) {
    this.timestamp = timestamp;
  }


  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

}
