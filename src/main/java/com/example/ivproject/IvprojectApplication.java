package com.example.ivproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IvprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(IvprojectApplication.class, args);
	}

}

